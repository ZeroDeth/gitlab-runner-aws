output "elb-name" {
  value       = aws_elb.runner-elb.name
  description = "The ID of the runner security group"
}
